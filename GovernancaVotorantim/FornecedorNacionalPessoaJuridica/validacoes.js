<script type="text/javascript">
         var divTelefone = $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent();
         var spanTelefone = divTelefone.find('span');
         var telefone = divTelefone.find('input');
     telefone.val('00 0000-0000');
         spanTelefone.attr('class','label label-danger ng-hide');

function configurarTelefone(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
      $('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}
var validaTelefone = function(){
     var divTelefone = $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent();
         var spanTelefone = divTelefone.find('span');
  
      if ($(this).val().length == 0) {
          $(this).css('background-color','#fff');
          $(this).val('00 0000-0000');
          spanTelefone.attr('class','label label-danger ng-hide');
      return false;
    }
      
      if ($(this).val().length >= 12){
            $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent().find('span').attr('class','label label-danger ng-hide');
        }else{
            $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent().find('span').attr('class','label label-danger');
        }
  
         var telefone = $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent().find('input');
    if(telefone.val().length != 12){
          telefone.css('background-color','#FFEFD5');
        alert("Telefone incompleto");
          telefone.focus();
       return false;
    }else{
      telefone.css('background-color','#fff');
    }
}

 configurarTelefone('Telefone','blur',validaTelefone);
         var divFax = $('label').filter(function () { return $(this).attr('for') == "Fax"}).parent();
         var spanFax = divFax.find('span');
         var fax = divFax.find('input');
     fax.val('00 0000-0000');
         spanFax.attr('class','label label-danger ng-hide');

function configurarFax(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
      $('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}
var validaFax = function(){
     var divFax = $('label').filter(function () { return $(this).attr('for') == "Fax"}).parent();
         var spanFax = divFax.find('span');
  
      if ($(this).val().length == 0) {
          $(this).css('background-color','#fff');
          $(this).val('00 0000-0000');
          spanFax.attr('class','label label-danger ng-hide');
      return false;
    }
      
      if ($(this).val().length >= 12){
            $('label').filter(function () { return $(this).attr('for') == "Fax"}).parent().find('span').attr('class','label label-danger ng-hide');
        }else{
            $('label').filter(function () { return $(this).attr('for') == "Fax"}).parent().find('span').attr('class','label label-danger');
        }
  
         var fax = $('label').filter(function () { return $(this).attr('for') == "Fax"}).parent().find('input');
    if(fax.val().length != 12){
          fax.css('background-color','#FFEFD5');
        alert("Fax incompleto");
          fax.focus();
       return false;
    }else{
      fax.css('background-color','#fff');
    }
}

 configurarFax('Fax','blur',validaFax);

         var divCelular = $('label').filter(function () { return $(this).attr('for') == "Celular"}).parent();
         var Celular = divCelular.find('input');
     Celular.val('00 00000-0000');

function configurarCelular(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
      $('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}


function TelefonesCelular(f){
  var f2 = ((""+f).replace(/\D/g, '') + '00000000000' ).substring(0,11);
  var m = f2.match(/^(\d{2})(\d{5})(\d{4})$/);

  return (!m) ? null : m[1] + ' ' + m[2] + '-' + m[3];
}

var validaCelular = function(){
  
      if ($(this).val().length == 0) {
          $(this).val('00 00000-0000');
        }else{
          $(this).val(TelefonesCelular($(this).val()));
      }
}

 configurarCelular('Celular','blur',validaCelular);
  
         var divTelefone1 = $('label').filter(function () { return $(this).attr('for') == "Telefone 1"}).parent();
         var Telefone1 = divTelefone1.find('input');
     Telefone1.val('00 0000-0000');

function configurarTelefone1(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
      $('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}


function Telefones1(f){
  var f2 = ((""+f).replace(/\D/g, '') + '0000000000' ).substring(0,10);
  var m = f2.match(/^(\d{2})(\d{4})(\d{4})$/);

  return (!m) ? null : m[1] + ' ' + m[2] + '-' + m[3];
}

var validaTelefone1 = function(){
  
      if ($(this).val().length == 0) {
          $(this).val('00 0000-0000');
        }else{
          $(this).val(Telefones1($(this).val()));
      }
}

 configurarTelefone1('Telefone 1','blur',validaTelefone1);
  
         var divTelefax = $('label').filter(function () { return $(this).attr('for') == "Telefax"}).parent();
         var Telefax = divTelefax.find('input');
     Telefax.val('00 0000-0000');

function configurarTelefax(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
      $('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}


function Telefaxs(f){
  var f2 = ((""+f).replace(/\D/g, '') + '0000000000' ).substring(0,10);
  var m = f2.match(/^(\d{2})(\d{4})(\d{4})$/);

  return (!m) ? null : m[1] + ' ' + m[2] + '-' + m[3];
}

var validaTelefax = function(){
  
      if ($(this).val().length == 0) {
          $(this).val('00 0000-0000');
        }else{
          $(this).val(Telefaxs($(this).val()));
      }
}

 configurarTelefax('Telefax','blur',validaTelefax);

         var divTelefoneVenda = $('label').filter(function () { return $(this).attr('for') == "Telefone Venda"}).parent();
         var TelefoneVenda = divTelefoneVenda.find('input');
     TelefoneVenda.val('00 0000-0000');

function configurarTelefoneVenda(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
      $('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}


function TelefonesVendas(f){
  var f2 = ((""+f).replace(/\D/g, '') + '0000000000' ).substring(0,10);
  var m = f2.match(/^(\d{2})(\d{4})(\d{4})$/);

  return (!m) ? null : m[1] + ' ' + m[2] + '-' + m[3];
}

var validaTelefoneVenda = function(){
  
      if ($(this).val().length == 0) {
          $(this).val('00 0000-0000');
        }else{
          $(this).val(TelefonesVendas($(this).val()));
      }
}

 configurarTelefoneVenda('Telefone Venda','blur',validaTelefoneVenda);
  
function configurarCampoCNPJ(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
      $('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}

var validaCNPJ = function(){
  var s;
    Form = document.form;
    if ($(this).val().length == 0) {
          $(this).css('background-color','#fff');  
      return false;
    }
    s = limpa_string($(this).val());


    if (s.length == 14) {
      if (valida_CGC($(this).val()) == false) {
              var inp = $('label').filter(function () { return $(this).attr('for') == "CNPJ"}).parent().find('input');
              inp.css('background-color','#FFEFD5');
        alert("O CNPJ não é válido!");
                 inp.focus();
        return false;
      }else {
              inp.focus();
              inp.css('background-color','#fff');}
    } else {
          var inp = $('label').filter(function () { return $(this).attr('for') == "CNPJ"}).parent().find('input');
          inp.css('background-color','#FFEFD5');
      alert("O CNPJ não é válido!");
                 inp.focus();
      return false;
    }
      var inp = $('label').filter(function () { return $(this).attr('for') == "CNPJ"}).parent().find('input');
      inp.css('background-color','#fff');
    
    return true;
            
  }
  function limpa_string(S) {

    var Digitos = "0123456789";
    var temp = "";
    var digito = "";
    for (var i = 0; i < S.length; i++) {
      digito = S.charAt(i);
      if (Digitos.indexOf(digito) >= 0) {
        temp = temp + digito
      }
    }
    return temp
  }
  function valida_CGC(s) {
    var i;
    s = limpa_string(s);
    var c = s.substr(0, 12);
    var dv = s.substr(12, 2);
    var d1 = 0;
    for (i = 0; i < 12; i++) {
      d1 += c.charAt(11 - i) * (2 + (i % 8));
    }
    if (d1 == 0)
      return false;
    d1 = 11 - (d1 % 11);
    if (d1 > 9)
      d1 = 0;
    if (dv.charAt(0) != d1) {
      return false;
    }
    d1 *= 2;
    for (i = 0; i < 12; i++) {
      d1 += c.charAt(11 - i) * (2 + ((i + 1) % 8));
    }
    d1 = 11 - (d1 % 11);
    if (d1 > 9)
      d1 = 0;
    if (dv.charAt(1) != d1) {
      return false;
    }
       var inp = $('label').filter(function () { return $(this).attr('for') == "CNPJ"}).parent().find('input');
      inp.css('background-color','#fff');
    return true;
}

configurarCampoCNPJ('CNPJ','blur',validaCNPJ);

var taxaCompensacao = $('label').filter(function () { return $(this).attr('for') == "Taxa Compensação"}).parent().find('input');
  	 taxaCompensacao .prop('checked', true);

var verificaFatura = $('label').filter(function () { return $(this).attr('for') == "Verif. Fatura Dupliada"}).parent().find('input');
  	 verificaFatura .prop('checked', true);

 </script>