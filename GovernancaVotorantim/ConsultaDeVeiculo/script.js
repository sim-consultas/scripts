<script type+"text/javascript">

$.get('https://bitbucket.org/devsimconsultas/scripts/raw/master/GovernancaVotorantim/Generics/ConsultaSincrona/loadermodal.css',function(css){
  $('head').append(css);
});

var fieldset1 = $('fieldset:nth(1)');
var fieldset2 = $('fieldset:nth(2)');
$('fieldset:nth(1)').remove();
$('fieldset:nth(2)').remove();
$('fieldset:nth(0)').parent().append('<div id="divcampos"></div>')
$("#divcampos").append(fieldset1).append(fieldset2);

var btn = '<button id="btnConsultar" class="btn btn-default ng-binding" >Consultar</button>';
var btns = $('button[ng-click="save();"]').parent().html();
$('button[ng-click="save();"]').parent().empty().append(btn).append(btns);
$('button[ng-click="save();"]').hide();

// Pega valor do array de objetos retornados do webservice
function valorCampoWS(data,campo){
 return $(data.getElementsByTagName("ParseDataColumn")).filter(
      function(){
       return $(this).find('Key').text() == campo;
      }
 ).find('Value').text();
}

// Pegar do formulário
function valorCampoForm(label){
 return $('label').filter(function(){ return $(this).attr('for') == label; }).parent().find('input').val();
}


// Passa valor para um campo texto do formulario
function setarValorCampo(campo,valor){
 $('label').filter(function(){ return $(this).attr('for') == campo; }).parent().find('input').val(valor);
}

$('#btnConsultar').on("click",function(){ 
      var codren = valorCampoForm('Cod. RENAVAM');
      var numcpf = valorCampoForm('CPF/CNPJ');
      
      if(!numcpf || !codren){
          if(!confirm('Alguns parâmetros estão nulos. Deseja continuar a consulta?')) return false;
      }
  
      var parametrosConsulta = "cpf=" + numcpf + "&" + "renavam=" + codren;    
      
      $(this).attr('disabled',"disabled");
  
      $.ajax( {
          data: { chaveacesso : "D16A043A-9359-44C7-9526-5DF8575FDC71", parser : "Denatran", parametros : parametrosConsulta },
          type:'Get',
          url:'http://lb.simconsultas.com.br/wsprincipal/service.asmx/ConsultaSincronaGC',
          beforeSend:function(){ 
            $('div[ng-show="executingAjax"]').hide();           
            $('#divcampos').addClass('col-sm-12 form-group ng-scope ModalContainer').append('<div id="spinloader" class="showbox"><div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke miterlimit="10"/></svg></div></div>');            
          },
          success:function(data) {             
            $('#divcampos').removeClass('col-sm-12 form-group ng-scope ModalContainer');
            $('#spinloader').remove();
            $('div[ng-show="executingAjax"]').show();

            $('#btnConsultar').removeAttr('disabled');                    
            var retornoErro = $(data.getElementsByTagName("ErrorDesc")).text(); 
            if(retornoErro.length > 0){
                alert(retornoErro);
                return ;
            }
            setarValorCampo('Placa',valorCampoWS(data,"placa")); 
            setarValorCampo('Código Renavam',valorCampoWS(data,"renavam")); 
            setarValorCampo('CPF/CNPJ',valorCampoWS(data,"cnpjcpf")); 
            setarValorCampo('Nome do Prioritário',valorCampoWS(data,"proprietario")); 
            setarValorCampo('Tipo',valorCampoWS(data,"tipo")); 
            setarValorCampo('Espécie',valorCampoWS(data,"especie")); 
            setarValorCampo('Carroceria',valorCampoWS(data,"carroceria")); 
            setarValorCampo('Categoria',valorCampoWS(data,"categoria")); 
            setarValorCampo('Combustivel',valorCampoWS(data,"combustivel")); 
            setarValorCampo('Marca',valorCampoWS(data,"marca")); 
            setarValorCampo('Modelo',valorCampoWS(data,"modelo"));
            setarValorCampo('Ano Fabricação',valorCampoWS(data,"anofabricacao"));
            setarValorCampo('Ano Modelo',valorCampoWS(data,"anomodelo"));
            setarValorCampo('Cor',valorCampoWS(data,"cor"));
            setarValorCampo('Lotação',valorCampoWS(data,"lotacao"));
            setarValorCampo('Capacidade de Carga',valorCampoWS(data,"capacidade"));
            setarValorCampo('Potência',valorCampoWS(data,"potencia"));
            setarValorCampo('Cilindradas',valorCampoWS(data,"cilindradas"));
            setarValorCampo('Restrição-1',valorCampoWS(data,"restricao1"));
            setarValorCampo('Restrição-2',valorCampoWS(data,"restricao2"));
            setarValorCampo('Restrição-3',valorCampoWS(data,"restricao3"));
            setarValorCampo('Restrição-4',valorCampoWS(data,"restricao4"));
            setarValorCampo('Existe Ocorrência de Roubo Ativa?',valorCampoWS(data,"roubo"));
            setarValorCampo('Existe Ocorrência de Furto Ativa?',valorCampoWS(data,"furto"));
            setarValorCampo('Existe Comunicação de Venda',valorCampoWS(data,"comunicacao_venda"));
            setarValorCampo('Existe Restrição Judicial RENAJUD?',valorCampoWS(data,"restricao_judicial_renajud"));
            setarValorCampo('Existe Multa Interestadual?',valorCampoWS(data,"multa_interestadual"));
            setarValorCampo('Existe Recall',valorCampoWS(data,"recall"));   
         },
         error: function(){            
            $('#divcampos').removeClass('ModalContainer');
            $('#spinloader').remove();
            $('div[ng-show="executingAjax"]').show();
            $('button[ng-click="save();"]').text("Consultar").removeAttr('disabled');
            alert('Não foi possivel fazer a consulta.');
            $('button[ng-click="save();"]').text("Consultar").removeAttr('disabled');
          }
      });
      return false;
});

window.setTimeout(function() {
    $(".alert-danger").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 5000);

</script>