<script type="text/javascript">


//--------------- VALIDAR CPF----------------------------------------------------------------------------------------------------------------------------
function configurarCampoCPF(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
   		$('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}
 
var validaCPF = function(){
    var inp = $('label').filter(function () { return $(this).attr('for') == "CPF"}).parent().find('input');
    //var cpf = inp.replace(/[^\d]+/g,'');    
    if(inp.val() == '' || inp.val().length == 0)
    {
      inp.css('background-color','#fff');
      return false;
    }
    // Elimina CPFs invalidos conhecidos    
    if (inp.val().length != 11 || inp.val().length < 11 ||
       inp.val() == "00000000000" || 
       inp.val() == "11111111111" || 
        inp.val() == "22222222222" || 
        inp.val() == "33333333333" || 
        inp.val() == "44444444444" || 
       inp.val() == "55555555555" || 
       inp.val() == "66666666666" || 
        inp.val() == "77777777777" || 
        inp.val() == "88888888888" || 
        inp.val() == "99999999999"){
           inp.css('background-color','#FFEFD5');
     		alert("CPF não é válido");
  			inp.focus();
     		 return false;
    }else{
  inp.css('background-color','#fff');        
        }


    // Valida 1o digito
  if (inp.val().length == 10){
    add = 0;    
    for (i=0; i < 9; i ++)       
        add += parseInt(inp.val().charAt(i)) * (10 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11)     
            rev = 0;    
        if (rev != parseInt(inp.val().charAt(9))){
        inp.css('background-color','#FFEFD5');
         alert("CPF não é válido");
  			inp.focus();  
        }else{
  inp.css('background-color','#fff');        
        }
  }
  
    // Valida 2o digito 
    if (inp.val().length == 11){
    add = 0;    
    for (i = 0; i < 10; i ++)        
        add += parseInt(inp.val().charAt(i)) * (11 - i);  
    rev = 11 - (add % 11);  
    if (rev == 10 || rev == 11) 
        rev = 0;    
    if (rev != parseInt(inp.val().charAt(10))){
            inp.css('background-color','#FFEFD5');
        alert("CPF não é válido");
  		inp.focus();
    	}else{
  inp.css('background-color','#fff');        
        }
    }
   return true; 
  inp.css('background-color','#fff');
    
}
configurarCampoCPF('CPF','blur',validaCPF);

//-----------------------------------------------------------------------------------------------------------------------------------------------------------


//TELEFONE OBRIGATÓRIO ---------------------------------------------------------------------------
       	 var divTelefone = $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent();
         var spanTelefone = divTelefone.find('span');
         var telefone = divTelefone.find('input');
		 telefone.val('00 0000-0000');
         spanTelefone.attr('class','label label-danger ng-hide');

function configurarTelefone(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
   		$('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}
var validaTelefone = function(){
 		 var divTelefone = $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent();
         var spanTelefone = divTelefone.find('span');
  
  		if ($(this).val().length == 0) {
          $(this).css('background-color','#fff');
          $(this).val('00 0000-0000');
          spanTelefone.attr('class','label label-danger ng-hide');
			return false;
		}
  		
  		if ($(this).val().length >= 12){
          	$('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent().find('span').attr('class','label label-danger ng-hide');
        }else{
            $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent().find('span').attr('class','label label-danger');
        }
  
       	 var telefone = $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent().find('input');
  	if(telefone.val().length != 12){
      		telefone.css('background-color','#FFEFD5');
    		alert("Telefone incompleto");
      		telefone.focus();
     	 return false;
    }else{
    	telefone.css('background-color','#fff');
    }
}

 configurarTelefone('Telefone','blur',validaTelefone);
  

//---------------------------------------------------------------------------------------------------------

// CELULAR OBRIGATÓRIO---------------------------------------------------------------------------
       	 var divCelular = $('label').filter(function () { return $(this).attr('for') == "Celular"}).parent();
         var spanCelular = divCelular.find('span');
         var Celular = divCelular.find('input');
		 Celular.val('00000000000');
      spanCelular.attr('class','label label-danger ng-hide');

function configurarCelular(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
   		$('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}


function validaCelular(){
  var divCelular = $('label').filter(function () { return $(this).attr('for') == "Celular"}).parent();
         var spanCelular = divCelular.find('span');

         if ($(this).val().length == 0) {
          $(this).css('background-color','#fff');
          $(this).val('00000000000');
          spanCelular.attr('class','label label-danger ng-hide');
      return false;
    }

    if ($(this).val().length >= 11){
            $('label').filter(function () { return $(this).attr('for') == "Celular"}).parent().find('span').attr('class','label label-danger ng-hide');
        }else{
            $('label').filter(function () { return $(this).attr('for') == "Celular"}).parent().find('span').attr('class','label label-danger');
        }

  var Celular = $('label').filter(function () { return $(this).attr('for') == "Celular"}).parent().find('input');
    if(Celular.val().length != 11){
          Celular.css('background-color','#FFEFD5');
        alert("Celular incompleto");
          Celular.focus();
       return false;
    }else{
      Celular.css('background-color','#fff');
    }
	
}


 configurarCelular('Celular','blur',validaCelular);
  

//---------------------------------------------------------------------------------------------------------
// CELULAR TRANSPORTADORA---------------------------------------------------------------------------
       	 var divCelularTransportadora = $('label').filter(function () { return $(this).attr('for') == "Celular Transportadora"}).parent();
         var celularTrans = divCelularTransportadora.find('input');
		 celularTrans.val('00000000000');

function configurarCelularTrans(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
   		$('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}


function CelularTrans(f){
	var f2 = ((""+f).replace(/\D/g, '') + '00000000000' ).substring(0,11);
	var m = f2.match(/^(\d{2})(\d{5})(\d{4})$/);

	return (!m) ? null : m[1] +  m[2] + m[3];
}

var validaelularTrans = function(){
  
  		if ($(this).val().length == 0) {
          $(this).val('00000000000');
        }else{
          $(this).val(CelularTrans($(this).val()));
  		}
}

 configurarCelularTrans('Celular Transportadora','blur',validaelularTrans);
  

//---------------------------------------------------------------------------------------------------------

</script>