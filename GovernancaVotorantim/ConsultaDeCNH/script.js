<script type+"text/javascript">

$.get('https://bitbucket.org/devsimconsultas/scripts/raw/master/GovernancaVotorantim/Generics/ConsultaSincrona/loadermodal.css',function(css){
  $('head').append(css);
});

var btn = '<button id="btnConsultar" class="btn btn-default ng-binding" >Consultar</button>';
// var btns = $('button[ng-click="save();"]').parent().html();
// $('button[ng-click="save();"]').parent().empty().append(btn).append(btns);
//$('button[ng-click="save();"]').hide();
$('button[ng-click="save();"]').parent().append(btn);

// Pega valor do array de objetos retornados do webservice
function valorCampoWS(data,campo){
 return $(data.getElementsByTagName("ParseDataColumn")).filter(
      function(){
       return $(this).find('Key').text() == campo;
      }
 ).find('Value').text();
}

// Pegar do formulário
function valorCampoForm(label){
 return $('label').filter(function(){ return $(this).attr('for') == label; }).parent().find('input').val();
}


// Passa valor para um campo texto do formulario
function setarValorCampo(campo,valor){
	var campo = $('label').filter(function(){ return $(this).attr('for') == campo; }).parent().find('input');
    $(campo).val(valor);
 
	var scope = $(campo).scope();
    scope.$apply(function(scope){
        scope.$parent.$$childHead.form.Data[$(campo).prop('id')] = valor;
    }); 
}

$('#btnConsultar').on("click",function(){ 
      var numCpf = valorCampoForm('CPF Do Condutor');
      var numReg = valorCampoForm('Número de Registro');
      var numseg = valorCampoForm('Número de Segurança Renach');

      if(!numCpf || !numReg || !numseg){
          if(!confirm('Alguns parâmetros estão nulos. Deseja continuar a consulta?')) return false;
      }
      
      $(this).attr('disabled',"disabled");
  
      var parametrosConsulta = "cpf=" + numCpf + "&" + "cnh=" + numReg + "&" + "numeroseguranca=" + numseg;
      $.ajax( {
          data: { chaveacesso : "D16A043A-9359-44C7-9526-5DF8575FDC71", Parser : "DENATRANCNH", Parametros : parametrosConsulta },
          type:'Get',
          url:'http://lb.simconsultas.com.br/wsprincipal/service.asmx/ConsultaSincronaGC',
          beforeSend:function(){ 
            $('div[ng-show="executingAjax"]').hide();           
            $('fieldset').filter(function(){return $(this).find('legend').text() == "Dados Consulta CNH" }).addClass('col-sm-12 form-group ng-scope ModalContainer').append('<div id="spinloader" class="showbox"><div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke miterlimit="10"/></svg></div></div>');            
          },
          success:function(data) {             
            $('fieldset').filter(function(){return $(this).find('legend').text() == "Dados Consulta CNH" }).removeClass('ModalContainer');
            $('#spinloader').remove();
            $('div[ng-show="executingAjax"]').show();

            $('#btnConsultar').removeAttr('disabled');                    
            var retornoErro = $(data.getElementsByTagName("ErrorDesc")).text(); 
            if(retornoErro.length > 0){
                alert(retornoErro);
                return ;
            }
            setarValorCampo('Nome da Mãe',valorCampoWS(data,"NomeMae"));
            setarValorCampo('Data Emissão CNH',valorCampoWS(data,"DataEmissaoCNH"));
            setarValorCampo('Nome do Condutor',valorCampoWS(data,"NomedoCondutor")); 
            setarValorCampo('Número do Registro',valorCampoWS(data,"NroRegistro"));  
            setarValorCampo('Numero CNH',valorCampoWS(data,"NroCNH"));
            setarValorCampo('Validade CNH',valorCampoWS(data,"ValidadeCNH"));  
            setarValorCampo('Categoria CNH',valorCampoWS(data,"Categoria")); 
            setarValorCampo('Categoria CNH',valorCampoWS(data,"Categoria")); 
            setarValorCampo('Numero de Segurança',valorCampoWS(data,"NroSeguranca"));    
			
			try{
				var id = location.hash.match(/(idEnvio=)(\d+)/)[2];
				setarValorCampo('ID',id);    
			}catch(msg){
				
			}
         },
         error: function(){            
            $('fieldset').filter(function(){return $(this).find('legend').text() == "Dados Consulta CNH" }).removeClass('ModalContainer');
            $('#spinloader').remove();
            $('div[ng-show="executingAjax"]').show();
            $('button[ng-click="save();"]').text("Consultar").removeAttr('disabled');
            alert('Não foi possivel fazer a consulta.');
            $('button[ng-click="save();"]').text("Consultar").removeAttr('disabled');
          }
      });
	  
      return false;
});

window.setTimeout(function() {
    $(".alert-danger").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
	

}, 5000);


</script>
