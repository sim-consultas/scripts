﻿<script type="text/javascript">

var divIDGrupoContas = $('label').filter(function(){ 
    return $(this).attr('for')== "38" 
}).parent();
var IDGrupoContas = divIDGrupoContas.find('input');
divIDGrupoContas.hide();

var lista = ['Criar / Revisar Cadastro',
'Número CNH',
'Categoria CNH',
'Reintegração?',
'Autorização',
'Qual Autorização',
'Tipo de Documento',
'Número do Documento',
'Órgão Emissor',
'Número do Documento (Outros)',
'Órgão Expedidor (Outros)',
'Responsável Inclusão',
'Responsável Alteração',
'Responsável Alteração Integração',
'Responsável Alteração Num Celular',
'Validade do Cadastro',
'Validade da CNH',
'Data Inclusão',
'Hora Inclusão',
'Data Alteração',
'Hora Alteração',
'Data Alteração Integração',
'Hora Alteração Integração',
'Data Alteração Num Celular',
'Hora Alteração Num Celular'];

$.each(lista,function(i,e){ $('label').filter(function(){ 
    return $(this).attr('for')== e 
}).parent().css('display','none'); });

function configurarCampoCPF(label, evento, funcao){
    for(var i = 0; i < label.split(';').length; i++){
        $('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
    }
}
 



var validaCPF = function(){
    var inp = $('label').filter(function () { return $(this).attr('for') == "CPF"}).parent().find('input');
    //var cpf = inp.replace(/[^\d]+/g,'');    
    if(inp.val() == '' || inp.val().length == 0)
    {
        inp.css('background-color','#fff');
        return false;
    }
    // Elimina CPFs invalidos conhecidos    
    if (inp.val().length != 11 || inp.val().length < 11 ||
       inp.val() == "00000000000" || 
       inp.val() == "11111111111" || 
        inp.val() == "22222222222" || 
        inp.val() == "33333333333" || 
        inp.val() == "44444444444" || 
       inp.val() == "55555555555" || 
       inp.val() == "66666666666" || 
        inp.val() == "77777777777" || 
        inp.val() == "88888888888" || 
        inp.val() == "99999999999"){
        inp.css('background-color','#FFEFD5');
        alert("CPF não é válido");
        inp.focus();
        return false;
    }else{
        inp.css('background-color','#fff');        
    }


    // Valida 1o digito
    if (inp.val().length == 10){
        add = 0;    
        for (i=0; i < 9; i ++)       
            add += parseInt(inp.val().charAt(i)) * (10 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11)     
            rev = 0;    
        if (rev != parseInt(inp.val().charAt(9))){
            inp.css('background-color','#FFEFD5');
            alert("CPF não é válido");
            inp.focus();  
        }else{
            inp.css('background-color','#fff');        
        }
    }
  
    // Valida 2o digito 
    if (inp.val().length == 11){
        add = 0;    
        for (i = 0; i < 10; i ++)        
            add += parseInt(inp.val().charAt(i)) * (11 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11) 
            rev = 0;    
        if (rev != parseInt(inp.val().charAt(10))){
            inp.css('background-color','#FFEFD5');
            alert("CPF não é válido");
            inp.focus();
        }else{
            inp.css('background-color','#fff');        
        }
    }
    return true; 
    inp.css('background-color','#fff');
    
}
configurarCampoCPF('CPF','blur',validaCPF);

var divTelefone = $('label').filter(function () { return $(this).attr('for') == "Telefone Fixo"}).parent();
var spanTelefone = divTelefone.find('span');
var telefone = divTelefone.find('input');
telefone.val('00 0000-0000');
spanTelefone.attr('class','label label-danger ng-hide');

function configurarTelefone(label, evento, funcao){
    for(var i = 0; i < label.split(';').length; i++){
        $('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
    }
}
var validaTelefone = function(){
    var divTelefone = $('label').filter(function () { return $(this).attr('for') == "Telefone Fixo"}).parent();
    var spanTelefone = divTelefone.find('span');
  
    if ($(this).val().length == 0) {
        $(this).css('background-color','#fff');
        $(this).val('00 0000-0000');
        spanTelefone.attr('class','label label-danger ng-hide');
        return false;
    }
  		
    if ($(this).val().length >= 12){
        $('label').filter(function () { return $(this).attr('for') == "Telefone Fixo"}).parent().find('span').attr('class','label label-danger ng-hide');
    }else{
        $('label').filter(function () { return $(this).attr('for') == "Telefone Fixo"}).parent().find('span').attr('class','label label-danger');
    }
  
    var telefone = $('label').filter(function () { return $(this).attr('for') == "Telefone Fixo"}).parent().find('input');
    if(telefone.val().length != 12){
        telefone.css('background-color','#FFEFD5');
        alert("Telefone incompleto");
        telefone.focus();
        return false;
    }else{
        telefone.css('background-color','#fff');
    }
}

configurarTelefone('Telefone Fixo','blur',validaTelefone);


var divCelularTransportadora = $('label').filter(function () { return $(this).attr('for') == "Celular Transportadora"}).parent();
var celularTrans = divCelularTransportadora.find('input');
celularTrans.val('00 00000-0000');

var condutorEstrangeiro = $('label').filter(function () { return $(this).attr('for') == "Condutor Estrangeiro"}).parent().find('select');
setTimeout(function(){ condutorEstrangeiro.val('Não'); }, 1500);

var intSeguranca = $('label').filter(function () { return $(this).attr('for') == "Possui Int Segurança"}).parent().find('select');
setTimeout(function(){ intSeguranca.val('Não');}, 1500);


$.each(['CPF'],function(i,e){
    var obj = $('label').filter(function(k,l){ return $(l).attr('for') == e }).parent().find('input');
    angular.element($(obj)).scope().c.Properties.filter(function(h,m){ return h.Name == 'Obrigatório' })[0].Value = 'S';
    angular.element($(obj)).scope().c.Properties.filter(function(h,m){ return h.Name == 'Obrigatório' })[1].Value = 'S';
    angular.element($(obj)).scope().$apply();
})

condutorEstrangeiro.on('change', function(){ 
    var obrigatorio = $('label').filter(function(){ return $(this).prop('for') == 'CPF'}).parent().find('span');
    if($(this).val() == "Sim"){
        obrigatorio.attr('class','label label-danger ng-hide');
        $.each(['CPF'],function(i,e){
            var obj = $('label').filter(function(k,l){ return $(l).attr('for') == e }).parent().find('input');
            angular.element($(obj)).scope().c.Properties.filter(function(h,m){ return h.Name == 'Obrigatório' })[0].Value = 'N';
            angular.element($(obj)).scope().c.Properties.filter(function(h,m){ return h.Name == 'Obrigatório' })[1].Value = 'N';
            angular.element($(obj)).scope().$apply();
        })
    }else{
        $.each(['CPF'],function(i,e){
            var obj = $('label').filter(function(k,l){ return $(l).attr('for') == e }).parent().find('input');
            angular.element($(obj)).scope().c.Properties.filter(function(h,m){ return h.Name == 'Obrigatório' })[0].Value = 'S';
            angular.element($(obj)).scope().c.Properties.filter(function(h,m){ return h.Name == 'Obrigatório' })[1].Value = 'S';
            angular.element($(obj)).scope().$apply();
        })
    }
});
var intSeguranca = $('label').filter(function () { return $(this).attr('for') == "Possui Int Segurança"}).parent().find('select');
intSeguranca.on('change', function(){ 
    var integracao = $('label').filter(function(){ return $(this).prop('for') == 'Qual Integração'}).parent().find('span');
    var dtIntegracao = $('label').filter(function(){ return $(this).prop('for') == 'Data de Realização da Integração'}).parent().find('span');
    var validadeInt = $('label').filter(function(){ return $(this).prop('for') == 'Validade Integração'}).parent().find('span');
  
    if($(this).val() == "Não"){
        integracao.attr('class','label label-danger ng-hide');
        $.each(['Qual Integração'],function(i,e){
            var obj = $('label').filter(function(k,l){ return $(l).attr('for') == e }).parent().find('textarea');
            angular.element($(obj)).scope().c.Properties.filter(function(h,m){ return h.Name == 'Obrigatório' })[0].Value = 'N';
            angular.element($(obj)).scope().c.Properties.filter(function(h,m){ return h.Name == 'Obrigatório' })[1].Value = 'N';
            angular.element($(obj)).scope().$apply();
        })
    }else{
        $.each(['Qual Integração'],function(i,e){
            var obj = $('label').filter(function(k,l){ return $(l).attr('for') == e }).parent().find('textarea');
            angular.element($(obj)).scope().c.Properties.filter(function(h,m){ return h.Name == 'Obrigatório' })[0].Value = 'S';
            angular.element($(obj)).scope().c.Properties.filter(function(h,m){ return h.Name == 'Obrigatório' })[1].Value = 'S';
            angular.element($(obj)).scope().$apply();
        })
    }
});




var dtIntegracao = $('label').filter(function () { return $(this).attr('for') == "Data de Realização da Integração"}).parent().find('input');
var validadeIntegracao = $('label').filter(function () { return $(this).attr('for') == "Validade Integração"}).parent().find('input');
dtIntegracao.attr('readOnly',true);
validadeIntegracao.attr('readOnly',true);

var hoje = new Date();

var mes = hoje.getMonth()+1; mesFormatado = (mes < 10 ? "0" + mes : mes);
var dia = hoje.getDate(); diaFormatado = (dia < 10 ? "0" + dia : dia);
var ano = hoje.getFullYear();

dtIntegracao.val(diaFormatado + '-' + mesFormatado + '-' + ano);

var seisMeses = new Date(new Date(hoje).setMonth(hoje.getMonth()+7));
var meses = seisMeses.getMonth(); meses = (meses < 10 ? "0" + meses : meses);
var anos = seisMeses.getFullYear(); 
var dias = seisMeses.getDate(); dias = (dias < 10 ? "0" + dias : dias);

validadeIntegracao.val(dias + "-" + meses + "-" + anos);

var campos = ['4008 Votorantim Cimentos N/NE','4014 Votorantim Cimentos S/A', '4075 Pedreira Pedra Negra ltda', '4161 Minerações e Construções'];
$.each(campos,function(i,e){ 
    var inp = $('label').filter(function () { return $(this).attr('for') == e}).parent().find('input');

    $(inp).on('click', function(){
        var verifica = false;
        $.each(campos,function(i,j){
            var chk= $('label').filter(function () { return $(this).attr('for') == j}).parent().find('input');  
            var span = $('label').filter(function () { return $(this).attr('for') == j}).parent().find('span'); 
            if($(chk).is(":checked")){
                verifica = true;
            }
        });
        if(verifica){
            $.each(campos,function(i,j){
                var span = $('label').filter(function () { return $(this).attr('for') == j}).parent().find('span');
                span.attr('class','label label-danger ng-hide');
            });
        }else{
            $.each(campos,function(i,j){
                var span = $('label').filter(function () { return $(this).attr('for') == j}).parent().find('span'); 
                span.attr('class','label label-danger');
            });
        }
    });
});

var configurarCampos = new function(){
    this.setarValorCampo = function (label,valor){
        var divPai = $('label').filter(function(){return $(this).prop('for') == label}).parent();
        $(divPai).find('input').val(valor);	
        $(divPai).find('span:nth(1)').addClass('label label-danger ng-hide');
    };
    this.setarMetodo = function (label,tipo,evento, metodo){
        $('label').filter(function(){return $(this).prop('for') == label}).parent().find(tipo).on(evento,metodo);
    };
    this.pegarValorCampoInput = function (label){
        return $('label').filter(function(){return $(this).prop('for') == label}).parent().find('input').val();	
    };
    this.pegarValorXML = function(xml,tag){
        var retorno = "";
        try{
            retorno = xml.getElementsByTagName(tag)[0].textContent;
        }catch(erro){
			
        }
        return retorno;
    };
    this.addLoaderFiedlset = function(legenda){
        var ele = $('legend').filter(function(){return $(this).text() == legenda}).parent();
        $('div[ng-show="executingAjax"]').hide();           
        $(ele).addClass('col-sm-12 form-group ng-scope ModalContainer').append('<div id="spinloader" class="showbox"><div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke miterlimit="10"/></svg></div></div>');     
    };
    this.removeLoaderFiedlset = function(legenda){
        var ele = $('legend').filter(function(){return $(this).text() == legenda}).parent();
        $('div[ng-show="executingAjax"]').hide();           
        $(ele).removeClass('ModalContainer');
        $('div[ng-show="executingAjax"]').show();         
        $('#spinloader').remove();
    };
    this.limparCamposInput = function(campos){
        $.each(campos,function(i,e){
            var divPai = $('label').filter(function(){return $(this).text() == e}).parent();	
            $(divPai).find('input').val('');	
        })
    };
    this.setarFocusCampo = function(label){
        var divPai = $('label').filter(function(){return $(this).text() == label}).parent();	
        $(divPai).find('input').focus();	
    }
}

var consultarCep = function(){
    var parCep = configurarCampos.pegarValorCampoInput('CEP').replace(/[^\w\s]/gi, '');
    if(parCep.length == 0) return;
    configurarCampos.limparCamposInput(['Rua','Número','Complemento','Bairro','Cidade','UF']);
    $.ajax({
        url: 'http://wshomologa.simconsultas.com.br/wsgc/service.asmx/BuscarCep',
        type: 'GET',
        data: {'cep' : parCep},
        beforeSend:function(){ 
            configurarCampos.addLoaderFiedlset('Complemento Condutor');
        },
        success: function(xml){
            configurarCampos.removeLoaderFiedlset('Complemento Condutor');
			
            if(xml.getElementsByTagName('CEP').length == 0){				
                alert('Cep não foi encontrado');
                configurarCampos.limparCamposInput(['CEP','Rua','Número','Complemento','Bairro','Cidade','UF']);
                configurarCampos.setarFocusCampo('CEP');
                return;
            }
				
			
            var _logradouro = configurarCampos.pegarValorXML(xml,'LOGRADOURO');
            var _endereco = configurarCampos.pegarValorXML(xml,'ENDERECO');
            var _bairro = configurarCampos.pegarValorXML(xml,'BAIRRO');
            var _cidade = configurarCampos.pegarValorXML(xml,'CIDADE');
            var _uf = configurarCampos.pegarValorXML(xml,'UF');
			
            configurarCampos.setarValorCampo('Rua', _logradouro + ' ' + _endereco);
            configurarCampos.setarValorCampo('Bairro', _bairro);
            configurarCampos.setarValorCampo('Cidade', _cidade);
            configurarCampos.setarValorCampo('UF', _uf);
        },
        error: function(erro){
            configurarCampos.removeLoaderFiedlset('Complemento Condutor');
            alert('Não foi possivel consultar o cep : ' + parCep);
        }
    });
}

configurarCampos.setarMetodo('CEP','input','blur',consultarCep);
</script>