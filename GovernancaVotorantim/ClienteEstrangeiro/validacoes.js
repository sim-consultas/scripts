<script type="text/javascript">
 

//MARCAR CHECKBOX COMO DEFAULT ----------------------------------------------------------------------------------------------------------------------------

	 var inp = $('label').filter(function () { return $(this).attr('for') == "Registrar Histórico de Pagamento"}).parent().find('input');
  	 inp.prop('checked', true);

//---------------------------------------------------------------------------------------------------------------------------------------------------------


//TELEFONE OBRIGATÓRIO ---------------------------------------------------------------------------
       	 var divTelefone = $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent();
         var spanTelefone = divTelefone.find('span');
         var telefone = divTelefone.find('input');
		 telefone.val('00 0000-0000');
         spanTelefone.attr('class','label label-danger ng-hide');

function configurarTelefone(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
   		$('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}
var validaTelefone = function(){
 		 var divTelefone = $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent();
         var spanTelefone = divTelefone.find('span');
  
  		if ($(this).val().length == 0) {
          $(this).css('background-color','#fff');
          $(this).val('00 0000-0000');
          spanTelefone.attr('class','label label-danger ng-hide');
			return false;
		}
  		
  		if ($(this).val().length >= 12){
          	$('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent().find('span').attr('class','label label-danger ng-hide');
        }else{
            $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent().find('span').attr('class','label label-danger');
        }
  
       	 var telefone = $('label').filter(function () { return $(this).attr('for') == "Telefone"}).parent().find('input');
  	if(telefone.val().length != 12){
      		telefone.css('background-color','#FFEFD5');
    		alert("Telefone incompleto");
      		telefone.focus();
     	 return false;
    }else{
    	telefone.css('background-color','#fff');
    }
}

 configurarTelefone('Telefone','blur',validaTelefone);
  

//---------------------------------------------------------------------------------------------------------

// FAX OBRIGATÓRIO---------------------------------------------------------------------------
       	 var divFax = $('label').filter(function () { return $(this).attr('for') == "Fax"}).parent();
         var spanFax = divFax.find('span');
         var fax = divFax.find('input');
		 fax.val('00 0000-0000');
         spanFax.attr('class','label label-danger ng-hide');

function configurarFax(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
   		$('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}
var validaFax = function(){
 		 var divFax = $('label').filter(function () { return $(this).attr('for') == "Fax"}).parent();
         var spanFax = divFax.find('span');
  
  		if ($(this).val().length == 0) {
          $(this).css('background-color','#fff');
          $(this).val('00 0000-0000');
          spanFax.attr('class','label label-danger ng-hide');
			return false;
		}
  		
  		if ($(this).val().length >= 12){
          	$('label').filter(function () { return $(this).attr('for') == "Fax"}).parent().find('span').attr('class','label label-danger ng-hide');
        }else{
            $('label').filter(function () { return $(this).attr('for') == "Fax"}).parent().find('span').attr('class','label label-danger');
        }
  
       	 var fax = $('label').filter(function () { return $(this).attr('for') == "Fax"}).parent().find('input');
  	if(fax.val().length != 12){
      		fax.css('background-color','#FFEFD5');
    		alert("Fax incompleto");
      		fax.focus();
     	 return false;
    }else{
    	fax.css('background-color','#fff');
    }
}

 configurarFax('Fax','blur',validaFax);
  

//---------------------------------------------------------------------------------------------------------


// FAX CONTATO ---------------------------------------------------------------------------
       	 var divFAX = $('label').filter(function () { return $(this).attr('for') == "FAX"}).parent();
         var FAX = divFAX.find('input');
		 FAX.val('00 0000-0000');

function configurarFAX(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
   		$('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}


function TelefonesFax(f){
	var f2 = ((""+f).replace(/\D/g, '') + '00000000000' ).substring(0,10);
	var m = f2.match(/^(\d{2})(\d{4})(\d{4})$/);

	return (!m) ? null : m[1] + ' ' + m[2] + '-' + m[3];
}

var validaFAX = function(){
  
  		if ($(this).val().length == 0) {
          $(this).val('00 0000-0000');
        }else{
          $(this).val(TelefonesFax($(this).val()));
  		}
}

 configurarFax('FAX','blur',validaFAX);
  

//---------------------------------------------------------------------------------------------------------
// CELULAR ---------------------------------------------------------------------------
       	 var divCelular = $('label').filter(function () { return $(this).attr('for') == "Celular"}).parent();
         var Celular = divCelular.find('input');
		 Celular.val('00 00000-0000');

function configurarCelular(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
   		$('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}


function TelefonesCelular(f){
	var f2 = ((""+f).replace(/\D/g, '') + '00000000000' ).substring(0,11);
	var m = f2.match(/^(\d{2})(\d{5})(\d{4})$/);

	return (!m) ? null : m[1] + ' ' + m[2] + '-' + m[3];
}

var validaCelular = function(){
  
  		if ($(this).val().length == 0) {
          $(this).val('00 00000-0000');
        }else{
          $(this).val(TelefonesCelular($(this).val()));
  		}
}

 configurarCelular('Celular','blur',validaCelular);
  

//---------------------------------------------------------------------------------------------------------
// TELEFONE 1 ---------------------------------------------------------------------------
       	 var divTelefone1 = $('label').filter(function () { return $(this).attr('for') == "Telefone 1"}).parent();
         var TELEFONE1 = divTelefone1.find('input');
		 TELEFONE1.val('00 0000-0000');

function configurarTELEFONE1(label, evento, funcao){
   for(var i = 0; i < label.split(';').length; i++){
   		$('label').filter(function(){return $(this).attr('for') == label.split(';')[i]}).parent().find('input').on(evento,funcao);
   }
}


function Telefones1(f){
	var f2 = ((""+f).replace(/\D/g, '') + '00000000000' ).substring(0,10);
	var m = f2.match(/^(\d{2})(\d{4})(\d{4})$/);

	return (!m) ? null : m[1] + ' ' + m[2] + '-' + m[3];
}

var validaTelefone = function(){
  
  		if ($(this).val().length == 0) {
          $(this).val('00 0000-0000');
        }else{
          $(this).val(Telefones1($(this).val()));
  		}
}

 configurarTELEFONE1('Telefone 1','blur',validaTelefone);
  

//---------------------------------------------------------------------------------------------------------

</script>